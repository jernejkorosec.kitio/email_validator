<?php

namespace email_validator\core;

require('email.object.php');
require('email.core.tools.php');

use phpDocumentor\Reflection\Types\This;
use email_validator\core\Email;


class CoreParser
{
    private $parseData;
    private $outputData;
    private $emailArr = array();
    private $emailObjArr = array();

    /*
        Getters and Setters
    */
    public function setOutputData($outputData){
        $this->outputData = $outputData;
    }
    public function getOutputData(){
        return $this->outputData;
    }
    public function setParseData($parsedata){
        $this->parseData = $parsedata;
    }
    public function getParseData(){
        return $this->parseData;
    }
    public function setEmailArr($emailarr){
        $this->emailArr = $emailarr;
    }
    public function getEmailArr(){
        return $this->emailArr;
    }
    

    /*
        Domain and MX records validation
    */

    private function validateEmail($string)
    {
        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $string, $matches);
        $rvalue = implode($matches[0]);
        return $rvalue;
    }

    private function extractDomain($email)
    {
        if( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
            $temp = explode('@', $email);
            $domain = array_pop($temp);
            return $domain;
        }
    }

    private function checkHost($domain)
    {
        if ( gethostbyname($domain) != $domain ) {
        return true;
        }
        else {
            return false;
        }
    }

    private function checkForMX($domain)
    {
        if ( checkdnsrr($domain.'.', 'MX') ) {
            return true;
        }
        else {
            return false;
        }
    }

    public function evaluate(Email $email)
    {
        //$post = $email->getEmail();
        $post = $email->getUnvalidatedEmail();
        $email->setValid(false);
        if(isset($post) && !empty($post))
        {
            $validatedEmail = $this->validateEmail($post);
            $domain = $this->extractDomain($validatedEmail);
            if(isset($validatedEmail) && !empty($validatedEmail))
            {
                $email->setEmail($validatedEmail);
                if(isset($domain) && !empty($domain))
                {
                    if($this->checkHost($domain))
                    {
                        $email->setDomain($domain);

                        if($this->checkForMX($domain))
                        {
                            $email->setMXrecord($domain);
                            $email->setValid(true);
                        }
                        else
                        {
                            $email->setMessage("Email Server for domain does not exists");
                        }
                    }
                    else
                    {
                        $email->setMessage("Domain for email does not exists");
                    }
                }
                else
                {
                    $email->setMessage("Domain is empty");
                }
            }
            else
            {
                $email->setMessage("Invalid email address");
            }
        }
        else
        {
            $email->setMessage("Empty Email String");
        }
    }

    /*
        POST And Array Functions
    */

    public function split($emailstext)
    {
        //echo $emailstext;
        //$ArrSubstr = preg_split("/\r\n|[\r\n]|;|[\s+]|[\t+]/", $emailstext);
        $ArrSubstr = preg_split("/\r\n|[\r\n]|;|[\s+][\t]/", $emailstext);
        //var_dump($ArrSubstr);
        $this->emailArr = array_filter($ArrSubstr);
        //var_dump($this->emailArr);
        return $this;
    }

    public function print()
    {
        foreach ($this->emailArr as $key => $value)
        {
            echo "{$key} => {$value} "; echo "<br/>";
        }
        return $this;
    }

    public function returnValidated()
    {
        if(!isset($textData))$textData='';
        foreach ($this->emailObjArr as $emailObject)  //$ value naj bi bil objekt
        {
            $textData .= $emailObject->getEmail() . "\n"; //echo "$textData";
        }
        //echo $textData;
        $this->setOutputData($textData);
        return $this;
    }


    

    public function printSize()
    {
        $size = sizeof($this->emailArr);
        echo "Size of Email Array is: $size";
        return $this;
    }

    public function validate($removefake = false)   // if set to true fake emails will not be in array of Email Objects
    {
        foreach ($this->emailArr as $key => $value)
        {
            $e = Email::withEmail($value);  //$e je dejanski objekt email
            $this -> evaluate($e);          //zapolni $e z podatki da se vidi ali je pravi ali ne
            if($e->getValid())
            {
                array_push($this->emailObjArr, $e); // ga doda na email
            }

        }
        // var_dump($this->emailObjArr);
        return $this;
    }

    public function dump()
    {
        Tools::prettyDump($this->emailObjArr);
        return $this;
    }

    public function prettyPrint()
    {
        foreach ($this->emailObjArr as $email)
        {
            echo "Email: ";
            Tools::sp(14);
            echo $email->getEmail();
            Tools::br();
            
            echo "Domena: ";
            Tools::sp(10);
            echo $email->getDomain();
            Tools::br();

            echo "Email strežnik: ";
            Tools::sp(0);
            echo $email->getMXrecord();
            Tools::br();

            if(!is_null( $email->getMessage()) )
            {
                echo "Napaka:";
                Tools::sp(12);
                echo $email->getMessage();
                Tools::br();
            }
            
            Tools::br();

        }
        return $this;
    }

    public function createForm($indexFile, $textAreaName)
    {
        echo $str = '
        <form action="'.$indexFile.'" method="post">
        <textarea name="'.$textAreaName.'" cols="100" rows="50"></textarea>
        <br/>
        <input type="submit" value="Preveri vnešene emaile">
        <input type="file" id="myFile" name="filename" value="Uvozi CSV Datoteko"> 
        </form>
        ';
        return $this;
    }
}
?>