<?php

namespace email_validator\core;

class CVClass
{
    private $postData;
    private $validatedEmail;
    private $domainName;
    private $emailServerName;
    
    public function setPostData($postadata){
        $this->postData = $postadata;
    }
    public function getPostData(){
        return $this->postData;
    }

    public function setValidatedEmail($validatedEmail){
        $this->validatedEmail = $validatedEmail;
    }
    public function getValidatedEmail(){
        return $this->ValidatedEmail;
    }

    public function setDomainName($domainName){
        $this->domainName = $domainName;
    }
    public function getDomainName(){
        return $this->domainName;
    }

    public function setEmailServerName($emailServerName){
        $this->emailServerName = $emailServerName;
    }
    public function getEmailServerName(){
        return $this->emailServerName;
    }
}

?>