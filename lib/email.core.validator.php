<?php

namespace email_validator\core;


class CoreValidator
{
    public static $my_static = 'CoreValidator';
    private $description = "Razred, ki vsebuje razne validator za email host in mx";
    private $version = "0.0.5";

    private $hostsExists;
    private $MXrecordExists;
    private $Email;

    private $Debug_Mode = true;

    public function setDebugMode($value)
    {
        settype($value, "boolean");
        $this->Debug_Mode = $value;
    }

    function __construct() 
    {
        
    }

    function eh($string)
    {
        if($this->Debug_Mode)
        {
            echo $string;
            echo "<br/>";
        }
    }

    function br()
    {
        if($this->Debug_Mode)
        {
            echo "<br/>";
        }
    }

    public function returnClassName()
    {
        return self::$my_static;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function validateEmail($string)
    {
        $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
        preg_match_all($pattern, $string, $matches);
        $rvalue = implode($matches[0]);
        return $rvalue;
    }

    public function extractDomain($email)
    {
        if( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
            $temp = explode('@', $email);
            $domain = array_pop($temp);
            return $domain;
        }
    }

    public function checkHost($domain)
    {
        if ( gethostbyname($domain) != $domain ) {
        return true;
        }
        else {
            return false;
        }
    }
    public function checkForMX($domain)
    {
        if ( checkdnsrr($domain.'.', 'MX') ) {
            return true;
        }
        else {
            return false;
        }
    }

    public function evaluateSingleEmail($post)
    {
        $cv = new CVClass();

        $cv->setPostData($post);

        if(isset($post) && !empty($post))
        {
            $this->eh("poslano preko post       : " . $post);
            
            $validatedEmail = $this->validateEmail($post);
           

            $this->eh("triman in validiran email: " . $validatedEmail);
            
            $domain = $this->extractDomain($validatedEmail);
            

            $this->eh("domena email streznika   : " . $domain);

            $this->eh("triman in validiran email: " . $validatedEmail. "... Se enkrat...");

            $this->br();


            if(isset($validatedEmail) && !empty($validatedEmail))
            {
                $cv->setValidatedEmail($validatedEmail);
                
                if(isset($domain) && !empty($domain))
                {
                    if($this->checkHost($domain))
                    {
                        $this->eh("ali domena http:\\\\$domain obstaja  : DA");
                        $cv->setDomainName($domain);
                        
                        if($this->checkForMX($domain))
                        {
                            $this->eh("ali email steznik domene obstaja : DA");
                            $cv->setEmailServerName($domain);
                        }
                        else
                        {
                            $this->eh("ali email steznik domene obstaja : NE");
                        }
                    }
                    else
                    {
                        $this->eh("ali domena http:\\\\$domain obstaja : NE");
                    }
                }
                else
                {
                    $this->eh("Domena je prazna");
                }
            }
            else
            {
                $this->eh("Neveljaven email naslov");
            }
        }
        else
        {
            $this->eh("poslali ste prazen string preko posta");
        }

        return $cv;
    }
}


?>
