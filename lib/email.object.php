<?php

namespace email_validator\core;

class Email
{
    private bool $valid;
    private $email;
    private $unvalidatedEmail;
    private $domain;
    private $MXrecord;
    private $Message;

    public function __construct() 
    {
        
    }

    // static methos like constructor with arguments
    public static function withEmail($_email) 
    {
        $instance = new self();
        $instance->unvalidatedEmail = $_email;
        return $instance;
        
    }

    public function setValid($Valid){
        $this->valid = $Valid;
    }
    public function getValid(){
        return $this->valid;
    }



    public function setEmail($email){
        $this->email = $email;
    }
    public function getEmail(){
        return $this->email;
    }

    /**
     * Get the value of domain
     */ 
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set the value of domain
     *
     * @return  self
     */ 
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get the value of MXrecord
     */ 
    public function getMXrecord()
    {
        return $this->MXrecord;
    }

    /**
     * Set the value of MXrecord
     *
     * @return  self
     */ 
    public function setMXrecord($MXrecord)
    {
        $this->MXrecord = $MXrecord;

        return $this;
    }

    /**
     * Get the value of unvalidatedEmail
     */ 
    public function getUnvalidatedEmail()
    {
        return $this->unvalidatedEmail;
    }

    /**
     * Set the value of unvalidatedEmail
     *
     * @return  self
     */ 
    public function setUnvalidatedEmail($unvalidatedEmail)
    {
        $this->unvalidatedEmail = $unvalidatedEmail;

        return $this;
    }

    /**
     * Get the value of Message
     */ 
    public function getMessage()
    {
        return $this->Message;
    }

    /**
     * Set the value of Message
     *
     * @return  self
     */ 
    public function setMessage($Message)
    {
        $this->Message = $Message;

        return $this;
    }
}

?>