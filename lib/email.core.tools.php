<?php

namespace email_validator\core;

class Tools
{
    private $longnewline =  "<br/>"."-------------------------------------"."<br/>";
    private static $newline =  "\n\r";
    private static $breakLine =  "<br/>";

    public function newLine()
    { 
        global $longnewline;
        echo $longnewline;
    }

    public function varDump($r1)
    { 
        echo '<pre>';
        var_dump($r1);
        echo '</pre>';
    }
    public static function prettyDump($r1)
    { 
        echo '<pre>';
        var_dump($r1);
        echo '</pre>';
    }

    public static function nLine()
    { 
        echo "\n\r";
    }

    public static function br()
    { 
        echo "<br/>";
    }

    public static function sp($j)
    { 
        for ($i=0; $i < $j; $i++) 
        { 
            echo " ";
        }
    }
}