<?php

namespace email_validator;

//require('lib\email.core.php');
//require('lib\email.core.exporter.php');

//$parser = new core\CoreParser();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php if (isset($title)) echo $title ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!-- CSS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.min.css">
    <style>
    input[type="file"] {
      display: none;
    }
    input[type="button"] {
      display: none;
    }
    

    .custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 2px 2px;
    cursor: pointer;
    height: 42px;
    }
    .export-to-file {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 2px 2px;
    cursor: pointer;
    height: 42px;
    }



   </style>
    <!-- JS -->
    <!--
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
    -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script>

    var exportData="";

    function download(data, filename, type) {
        var file = new Blob([data], {type: type});
        if (window.navigator.msSaveOrOpenBlob) // IE10+
            window.navigator.msSaveOrOpenBlob(file, filename);
        else { // Others
            var a = document.createElement("a"),
                    url = URL.createObjectURL(file);
            a.href = url;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            setTimeout(function() {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);  
            }, 0); 
        }
    }





      var openFile = function(event) {
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
          var text = reader.result;
          /* possible attributes of textera are val text and value */
          // $('#ids').val(text);                                     // Jquery style
          // document.getElementsByName("ids")[0].value = text;       // JS style
          // document.getElementsByName("ids")[0].innerHTML  = text;  // JS style
          // document.getElementsByName("ids")[0].innerText  = text;  // JS style
          $('#ids').val(text);                                     // Recommended
        };
        reader.readAsText(input.files[0]);
      };

    $(document).ready(function() {
      
      $("#target").submit(function(e) {   //target form name

        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this); 
        $.ajax({
              type: "POST",
              url: 'lib/email.core.exporter.php',
              data: form.serialize(), // serializes the form's elements.
              success: function(data)
              {
                //console.log(data);
                $('#ids').val(data);
                  exportData = $('#ids').val();
              }
            });
        });

    }); 
    </script>
</head>
<body>

<form id="target" action="index.php" method="post">
  <textarea name="ids" id="ids" cols="100" rows="50"></textarea>
  <br/>
  <input type="submit" value="Preveri vnešene emaile">
  <label class="custom-file-upload">
    <input type='file' id='file' id='file' accept='text/plain, .csv, .txt' onchange='openFile(event)' text='CSV'>
    <img src="/img/btncsv.png" alt="Snow">
  </label>
  <label class="export-to-file">
  <input type='button' id='downloadinput' name='downloadinput'  onclick="download(exportData, 'exportedCSVEmail.txt', 'text/plain');"/>
    <img src="/img/btncsvexport.png" alt="Snow">
  </label>
</form>
